# EASY

# Define a method that, given a sentence, returns a hash of each of the words as
# keys with their lengths as values. Assume the argument lacks punctuation.
def word_lengths(str)
  word_length = Hash.new
  str.split.each { |word| word_length[word] = word.length }
  word_length
end

# Define a method that, given a hash with integers as values, returns the key
# with the largest value.
def greatest_key_by_val(hash)
  nest_array = hash.sort_by { |_, v| v }.last.first
  #hash.to_a.max_by { |nest| nest[-1]}[0]

end

# Define a method that accepts two hashes as arguments: an older inventory and a
# newer one. The method should update keys in the older inventory with values
# from the newer one as well as add new key-value pairs to the older inventory.
# The method should return the older inventory as a result. march = {rubies: 10,
# emeralds: 14, diamonds: 2} april = {emeralds: 27, moonstones: 5}
# update_inventory(march, april) => {rubies: 10, emeralds: 27, diamonds: 2,
# moonstones: 5}
def update_inventory(older, newer)
  newer.each { |k, v| older[k] = v }
  older
end

# Define a method that, given a word, returns a hash with the letters in the
# word as keys and the frequencies of the letters as values.
def letter_counts(word)
  freq = Hash.new(0)
  word.chars.each { |ch| freq[ch] += 1 }
  freq
end

# MEDIUM

# Define a method that, given an array, returns that array without duplicates.
# Use a hash! Don't use the uniq method.
def my_uniq(arr)
  counter = Hash.new(0)
  arr.each { |el| counter[el] += 1 }
  counter.keys
end

# Define a method that, given an array of numbers, returns a hash with "even"
# and "odd" as keys and the frequency of each parity as values.
def evens_and_odds(numbers)
  parity = Hash.new(0)
  numbers.each do |int|
    parity[:even] += 1 if int.even?
    parity[:odd] += 1 if int.odd?
  end
  parity
end

# Define a method that, given a string, returns the most common vowel. If
# there's a tie, return the vowel that occurs earlier in the alphabet. Assume
# all letters are lower case.
def most_common_vowel(string)
  vwl_freq = Hash.new(0)
  string.chars.each { |ch| vwl_freq[ch] += 1 if "aeiou".include? ch }
  vwl_freq.select! { |_, v| v if v == vwl_freq.values.max }
  vwl_freq.keys.sort.first

end

# HARD

# Define a method that, given a hash with keys as student names and values as
# their birthday months (numerically, e.g., 1 corresponds to January), returns
# every combination of students whose birthdays fall in the second half of the
# year (months 7-12). students_with_birthdays = { "Asher" => 6, "Bertie" => 11,
# "Dottie" => 8, "Warren" => 9 }
# fall_and_winter_birthdays(students_with_birthdays) => [ ["Bertie", "Dottie"],
# ["Bertie", "Warren"], ["Dottie", "Warren"] ]
def fall_and_winter_birthdays(students)
  names_in_fall_and_winter = students.select { |_, v| v > 6 }.keys
  names_in_fall_and_winter.combination(2)
end

# Define a method that, given an array of specimens, returns the biodiversity
# index as defined by the following formula: number_of_species**2 *
# smallest_population_size / largest_population_size biodiversity_index(["cat",
# "cat", "cat"]) => 1 biodiversity_index(["cat", "leopard-spotted ferret",
# "dog"]) => 9
def biodiversity_index(specimens)
  number_of_species = specimens.uniq.count
  population_size_hsh = Hash.new(0)
  specimens.each { |animal| population_size_hsh[animal] += 1 }
  largest_population_size = population_size_hsh.max_by { |_, v| v }.last
  smallest_population_size = population_size_hsh.min_by { |_, v| v }.last
  number_of_species**2 * smallest_population_size / largest_population_size
end

# Define a method that, given the string of a respectable business sign, returns
# a boolean indicating whether pranksters can make a given vandalized string
# using the available letters. Ignore capitalization and punctuation.
# can_tweak_sign("We're having a yellow ferret sale for a good cause over at the
# pet shop!", "Leopard ferrets forever yo") => true
def can_tweak_sign?(normal_sign, vandalized_sign)
  chars_in_norm_sign = Hash.new(0)
  chars_in_vand_sign = Hash.new(0)

  normal_sign = punctuation_cleaner(normal_sign).downcase
  vandalized_sign = punctuation_cleaner(vandalized_sign).downcase

  normal_sign.chars.each { |ch| chars_in_norm_sign[ch] += 1 }
  vandalized_sign.chars.each { |ch| chars_in_vand_sign[ch] += 1 }

  chars_in_vand_sign.all? do |ch, freq|
    if chars_in_norm_sign.key?(ch) && (chars_in_norm_sign[ch] >= freq)
      true
    else false
    end
  end
end

def punctuation_cleaner(str)
  str.delete(".,;:!?")
end
